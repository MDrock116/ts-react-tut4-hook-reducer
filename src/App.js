//https://www.udemy.com/course/typescript-with-react-hooks-and-context/learn/lecture/13818734#overview
import React from 'react';
function App() {
    const reducer = (state, action) => {
        // if(action.type==='ADD'){return state+1}  //using if statements
        // if(action.type==='SUB'){return state-1}
        // if(action.type==='RES'){return 0}

        // const obj = {'ADD': state+1, 'SUB': state-1, 'RES': 0} //using array
        // return obj[action.type]

        switch (action.type) {  //using switch
            case 'ADD': return state+1
            case 'SUB': return state-1
            case 'RES': return 0
            default: return state
        }
    }
    const [count, dispatch] = React.useReducer(reducer, 0)
    return (
        <>
            <div>{count}</div>
            <button onClick={() => dispatch({type: 'ADD', someOtherVar: 0})}>+</button>
            <button onClick={() => dispatch({type: 'SUB', someOtherVar: 1})}>-</button>
            <button onClick={() => dispatch({type: 'RES', someOtherVar: 2})}>reset</button>
        </>
    );


}

export default App;
